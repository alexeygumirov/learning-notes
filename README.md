# Learning notes

This is the project where I share notes I make for different videos and courses.


## Youtube

[GOTO 2019: Lies, Damned Lies and Metrics](Youtube/GOTO2019-Lies-Damned-Lies-and-Metrics_Roy_Osherove.pdf)

[GOTO 2020: The Coaching Leader and Architect](Youtube/GOTO2020-The-Coaching-Leader-And-Architect_Roy_Osherove.pdf)

[FOSDM2020: Endless networ programming - An update from eBPF](Youtube/FOSDM2020-Endless-Network-Programming-An-Update-from-eBPF.pdf)

[Geek's Lesson: System Design Course for Beginners](Youtube/Geeks-Lesson-System-Design-Course-Beginners.pdf)

[Continuous Delivery: The problem with microservices](Youtube/CD-The-problem-with-microservices.pdf)

[Continuous Delivery: How to estimate SW develoment time](Youtube/CD-How-To-Estimate-Software-Development-Time.pdf)

## Udemy

[Google Associate Cloud Engineer: Get Certified 2020](Udemy/Google Accosicate Cloud Engineer - Get Certified 2020/GC_Course_Udemy_notes.pdf)

[Kubernetes for absolute beginners](Udemy/Kubernetes_for_the_Absolute_Beginners/k8s_4_beginners_Course_Udemy_notes.pdf)
